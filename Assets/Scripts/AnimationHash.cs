using UnityEngine;

public class AnimationHash
{
    public static readonly int IDLE = Animator.StringToHash("Idle");
    public static readonly int ATTACK = Animator.StringToHash("Attack");
    public static readonly int DIE = Animator.StringToHash("Die");
}