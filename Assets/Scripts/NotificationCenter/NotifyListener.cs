public interface NotifyListener
{
    void onNotify(string eventId, params object[] args);
}