using UnityEngine;

public class HpBar:MonoBehaviour
{
    private float current;
    private float max;
    [SerializeField]
    private Transform bar;

    public void init(float current, float max)
    {
        this.current = current;
        this.max = max;
        gameObject.SetActive(false);
    }

    public void setCurrent(float current)
    {
        this.current = current;

        if(!gameObject.activeSelf)
        {
            gameObject.SetActive(true);
        }

        Vector3 curScale = bar.localScale;
        curScale.x = current / max;
        bar.localScale = curScale;
    }
}