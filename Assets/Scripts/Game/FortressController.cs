using UnityEngine;

public class FortressController:MonoBehaviour, IDamageable, NotifyListener
{
    [SerializeField]
    private GameObject[] frames;
    [SerializeField]
    private float health = 1f;
    private float maxHealth;
    private bool active = true;
    private int damageLevel = 0;

    [SerializeField]
    private ObjectPool arrowPool;

    private void Start()
    {
        maxHealth = health;
        NotificationCenter.addListener("damageFort", this);
    }
    
    public void hit(float value)
    {
        if(!active)
        {
            return;
        }

        health -= value;
        if(health < 0f)
        {
            health = 0f;
        }

        updateGraph();       

        if(damageLevel >= (frames.Length - 1))
        {
            lose();
        }
    }

    private void updateGraph()
    {
        float percent = health / maxHealth;
        
        switch(damageLevel)
        {
            case 0:
                if(percent <= 0.85f)
                {
                    nextDamageLevel();
                }
            break;

            case 1:
                if(percent <= 0.5f)
                {
                    nextDamageLevel();
                }
            break;

            case 2:
                if(percent <= 0.15f)
                {
                    nextDamageLevel();
                }
            break;

            case 3:
                if(percent <= 0f)
                {
                    nextDamageLevel();
                }
            break;
        }
    }

    private void nextDamageLevel()
    {
        int currentLevel = damageLevel;
        damageLevel++;
        frames[currentLevel].SetActive(false);
        frames[damageLevel].SetActive(true);
    }

    private void lose()
    {
        active = false;

        NotificationCenter.sendNotify("showMenu", "lose");
    }

    public void onNotify(string eventId, params object[] args)
    {
        if(active)
        {
            hit((float)args[0]);
        }
    }
}