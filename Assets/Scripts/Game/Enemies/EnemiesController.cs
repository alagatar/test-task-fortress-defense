using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemiesController:MonoBehaviour, NotifyListener
{
    [SerializeField]
    private ObjectPool[] unitsData;
    [SerializeField]
    private float minOffset = 0f;
    [SerializeField]
    private float maxOffset = 0f;
    [SerializeField]
    private float moveDist = 1f;
    [SerializeField]
    private float startTimeout = 0f;
    [SerializeField]
    private float createTimeout = 1f;
    [SerializeField]
    private int countEnemies = 1;
    private int enemiesDestroyed = 0;

    private void Start()
    {
        NotificationCenter.addListener("destroyEnemie", this);
        StartCoroutine(creationRoutine());
    }

    private IEnumerator creationRoutine()
    {
        yield return new WaitForSeconds(startTimeout);

        for(int i = 0; i < countEnemies; i++)
        {
            createRandomEnemie();
            yield return new WaitForSeconds(createTimeout);
        }
    }

    private void createRandomEnemie()
    {
        int enemieId = Random.Range(0, unitsData.Length);
        ObjectPool pool = unitsData[enemieId];

        GameObject newEnemie = pool.pop();

        Vector2 startPosition = new Vector2();
        startPosition.y = Random.Range(minOffset, maxOffset);

        newEnemie.transform.SetParent(transform, false);
        newEnemie.transform.localPosition = startPosition;
        
        Enemie controller = newEnemie.GetComponent<Enemie>();
        controller.init(pool, moveDist);
    }

    public void onNotify(string eventId, params object[] args)
    {
        checkCompleteWave();
    }

    private void checkCompleteWave()
    {
        enemiesDestroyed++;
        if(enemiesDestroyed == countEnemies)
        {
            NotificationCenter.sendNotify("showMenu", "win");
        }
    }
}