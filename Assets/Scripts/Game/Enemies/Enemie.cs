using System;
using UnityEngine;
using System.Collections;

public class Enemie:MonoBehaviour, IDamageable
{
    [SerializeField]
    private float health = 1f;
    [SerializeField]
    private Animator animator;
    [SerializeField]
    private float speed = 1f;
    [SerializeField]
    private float attackTimeout = 1f;
    [SerializeField]
    private float damage = 1f;
    [SerializeField]
    private float dieDelay = 1f;
    [SerializeField]
    private HpBar hpBar;

    private Vector3 targetPoint;
    private bool mooving = true;
    private float timer;
    private bool active = true;
    private float maxHealth;
    private ObjectPool pool;

    private void Awake()
    {
        maxHealth = health;
    }

    public void init(ObjectPool pool, float moveDist)
    {
        this.pool = pool;
        health = maxHealth;
        targetPoint = transform.position;
        targetPoint.x += moveDist;
        timer = 0f;
        active = true;
        mooving = true;
        animator.SetTrigger(AnimationHash.IDLE);
        hpBar.init(health, maxHealth);
    }

    private void Update()
    {
        if(!active)
        {
            return;
        }

        if(mooving)
        {
            move();
        }
        else
        {
            attack();
        }
    }

    private void move()
    {
        if(checkCompleteMove())
        {
            startAttack();
            return;
        }

        float step = speed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, targetPoint, step);
    }

    private bool checkCompleteMove()
    {
        float dist = Math.Abs(transform.position.x - targetPoint.x);
        if(dist <= 0.1f)
        {
            return true;
        }
        return false;
    }

    private void startAttack()
    {
        mooving = false;
        animator.SetTrigger(AnimationHash.ATTACK);
    }

    private void attack()
    {
        timer += Time.deltaTime;
        if(timer >= attackTimeout)
        {
            timer = 0f;
            hitTarget();
        }
    }

    private void hitTarget()
    {
        NotificationCenter.sendNotify("damageFort", damage);
    }

    public void hit(float value)
    {
        if(!active)
        {
            return;
        }
        
        health -= value;
        if(health < 0f)
        {
            health = 0f;
        }

        hpBar.setCurrent(health);

        if(health <= 0f)
        {
            startDie();
        }
    }

    private void startDie()
    {
        active = false;
        animator.SetTrigger(AnimationHash.DIE);

        StartCoroutine(dieRoutine());
    }

    private IEnumerator dieRoutine()
    {
        yield return new WaitForSeconds(dieDelay);

        gameObject.SetActive(false);
        pool.push(gameObject);

        NotificationCenter.sendNotify("destroyEnemie");
    }
}