using UnityEngine;
using DG.Tweening;
using System;
using System.Collections;

public class Arrow:MonoBehaviour
{
    private float damage;
    private bool active = false;
    private Vector3 prevPosition;
    private ObjectPool pool;

    public void init(ObjectPool pool, float damage)
    {
        this.pool = pool;
        this.damage = damage;
    }

    public void throwTo(Vector3 pos)
    {
        active = true;
        prevPosition = transform.position;
        transform.DOJump(pos, 2.5f, 1, 0.4f).OnComplete(completeMove).SetEase(Ease.Flash).SetSpeedBased();
    }

    private void completeMove()
    {
        active = false;
        StartCoroutine(dieRoutine());
    }

    private IEnumerator dieRoutine()
    {
        yield return new WaitForSeconds(0.5f);

        moveToPool();
    }

    private void moveToPool()
    {
        gameObject.SetActive(false);
        pool.push(gameObject);
    }

    private void Update()
    {
        if(active)
        {
            updateRotation();
        }
    }

    private void updateRotation()
    {
        Vector3 dir = transform.position - prevPosition;
        prevPosition = transform.position;
       
        if(dir != Vector3.zero) 
        {
            float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
            angle += 180f;
            transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(!active)
        {
            return;
        }
        active = false;

        DOTween.Kill(transform);
        moveToPool();

        damageTarget(other.gameObject);
    }

    private void damageTarget(GameObject gameObject)
    {
        IDamageable controller = gameObject.GetComponent<IDamageable>();
        controller.hit(damage);
    }
}