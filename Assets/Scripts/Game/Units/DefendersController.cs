using UnityEngine;

public class DefendersController:MonoBehaviour
{
    [SerializeField]
    private float areaBorderLeft = 1f;
    [SerializeField]
    private float areaBorderRight = 1f;
    [SerializeField]
    private float groundPosition = 1f;
    [SerializeField]
    private ObjectPool arrowPool;
    [SerializeField]
    private Defender[] defenders;

    private void Start()
    {
        for(int i = 0; i < defenders.Length; i++)
        {
            defenders[i].init(arrowPool);
        }
    }

    private void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            shoot();
        }
    }

    private void shoot()
    {
        Vector3 targetPoint = getInputPoint();
        for(int i = 0; i < defenders.Length; i++)
        {
            defenders[i].shootTo(targetPoint);
        }
    }

    private Vector3 getInputPoint()
    {
        Vector3 pos = Input.mousePosition;
        pos = Camera.main.ScreenToWorldPoint(pos);
                
        pos.x = Mathf.Clamp(pos.x, areaBorderLeft, areaBorderRight);
        pos.y = groundPosition;
        pos.z = 0f;

        return pos;
    }
}