using UnityEngine;
using System.Collections;

public class Defender:MonoBehaviour
{
    [SerializeField]
    private Animator animator;
    [SerializeField]
    private float damage = 1f;
    [SerializeField]
    private float attackAnimDuration = 1f;
    [SerializeField]
    private Transform arrowOrigin;
    [SerializeField]
    private float randomRadius = 1f;
    [SerializeField]
    private float cooldown = 0.1f;
    private ObjectPool arrowPool;
    private Vector3 targetPoint;
    private bool attacking = false;

    public void init(ObjectPool pool)
    {
        arrowPool = pool;
    }

    public void shootTo(Vector3 targetPoint)
    {
        if(attacking)
        {
            return;
        }
        attacking = true;

        this.targetPoint = randomize(targetPoint);
        animator.SetTrigger(AnimationHash.ATTACK);

        StartCoroutine(attackRoutine());
    }

    private IEnumerator attackRoutine()
    {
        yield return new WaitForSeconds(attackAnimDuration);

        attack();

        yield return new WaitForSeconds(cooldown);

        attacking = false;
    }

    private void attack()
    {
        GameObject obj = arrowPool.pop();
        obj.transform.position = arrowOrigin.position;
        Arrow controller = obj.GetComponent<Arrow>();
        controller.init(arrowPool, damage);
        controller.throwTo(targetPoint);
    }

    private Vector3 randomize(Vector3 point)
    {
        Vector2 randomCircle = Random.insideUnitCircle * randomRadius;
        point.x += randomCircle.x;
        point.y += randomCircle.y;
        return point;
    }
}