public interface IDamageable
{
    void hit(float value);
}