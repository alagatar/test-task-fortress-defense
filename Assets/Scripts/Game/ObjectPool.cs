using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public class ObjectPool
{
    [SerializeField]
    private GameObject prefab;
    private List<GameObject> cache;

    public ObjectPool()
    {
        cache = new List<GameObject>();
    }

    public void push(GameObject obj)
    {
        cache.Add(obj);
    }

    public GameObject pop()
    {
        GameObject newObj;
        if(cache.Count > 0)
        {
            int index = cache.Count - 1;
            newObj = cache[index];
            cache.RemoveAt(index);

            newObj.SetActive(true);
        }
        else
        {
            newObj = GameObject.Instantiate(prefab);
        }
        return newObj;
    }
}