using System;
using UnityEngine;

public class GameScene:MonoBehaviour, NotifyListener
{
    [SerializeField]
    private GameObject loseMenu;
    [SerializeField]
    private GameObject winMenu;

    private void Start()
    {
        NotificationCenter.addListener("showMenu", this);
    }

    private void OnDestroy()
    {
        NotificationCenter.clear();
    }

    public void onNotify(string eventId, params object[] args)
    {
        showMenu((string)args[0]);
    }

    private void showMenu(string menuId)
    {
        if(menuId == "lose")
        {
            showLose();
        }
        else if(menuId == "win")
        {
            showWin();
        }
    }

    public void quit()
    {
        SceneLoadManager.instance.loadScene("Main Menu");
    }

    private void showLose()
    {
        loseMenu.SetActive(true);
    }

    private void showWin()
    {
        winMenu.SetActive(true);
    }
}