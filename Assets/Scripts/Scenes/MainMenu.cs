﻿using UnityEngine;

public class MainMenu:MonoBehaviour 
{
	public void startGame()
	{
		SceneLoadManager.instance.loadScene("Game Scene");
	}

	public void moreGames()
	{
		print("Show more games");
	}

	public void quitGame()
	{
		Application.Quit();
	}
}